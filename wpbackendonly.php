<?php
/**
 * WP Backend Only
 *
 * @package     wpbackendonly
 * @author      Michael Pilsner
 * @copyright   2017 Michael Pilsner
 * @license     GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: WP Backend Only
 * Plugin URI:  https://michaelpilsner.com
 * Description: Redirect all non-admin loggedin users to backend only.
 * Version:     1.0.2
 * Author:      Michael Pilsner
 * Author URI:  https://michaelpilsner.com
 * Text Domain: wp-backend-only
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

class wpbackendonly {
	static function checkuser() {

	// Check if user is logged in and not viewing backend
		if(is_user_logged_in() and !is_admin()) {
		 
            // Define current user to do checks for access and roles
			$user = wp_get_current_user();

		    // The roles listed here will be redirected to /wp-admin 
			$redirect_roles = array('administrator', 'editor', 'author');

			if( array_intersect($redirect_roles, $user->roles ) ) { 
				header("Location: ".get_admin_url());
				exit();
			}

		}

		else {
		// do nothing, user is not logged in.
		}
	}
}
// Run as soon as the site loads but before header sent
add_action( 'init', array('wpbackendonly', 'checkuser' ));